from flask import Flask, render_template, request, session
from flask_socketio import SocketIO, join_room, leave_room, emit, close_room

import eventlet
eventlet.monkey_patch()

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode='eventlet')

@app.route('/')
def index():
    return render_template('index.html')

NAMESPACE = '/chat'
users = []

# Socket io default events
@socketio.on('connect', namespace=NAMESPACE)
def handle_connect_event():
    # The room is used to send message to only this client
    # client_id = session.get("client_id", None) or request.sid
    client_id = request.sid
    join_room(client_id)

@socketio.on('register', namespace=NAMESPACE)
def handle_register(data):
    session['name'] = data['name']
    user = { 'client_id' : request.sid, 'name' : data['name'] }
    users.append(user)

    emit('registered', user)
    emit('online users', users)
    emit('new user', user, broadcast=True, include_self=False)

# Socket io default events
@socketio.on('rename', namespace=NAMESPACE)
def handle_rename(data):
    # The room is used to send message to only this client
    # client_id = session.get("client_id", None) or request.sid
    client_id = request.sid
    client = None

    for user in users:
        if user['client_id'] == client_id:
            client = user
            break

    if client is not None:
        client['name'] = data['name']
        session['name'] = data['name']
        emit('online users', users)

@socketio.on('broadcast message', namespace=NAMESPACE)
def handle_broadcast_message(data):
    emit('broadcast message', {
            'client_id' : request.sid,
            'name' : session['name'],
            'message' : data['message']
        }, broadcast=True, include_self=True)

@socketio.on('disconnect', namespace=NAMESPACE)
def handle_disconnect_event():
    # client_id = session.get("client_id", None) or request.sid
    client_id = request.sid
    close_room(client_id)

    client = None
    for user in users:
        if user['client_id'] == client_id:
            client = user
            emit('user offline', client, broadcast=True, include_self=False)
            users.remove(client)

if __name__ == '__main__':
    socketio.run(app=app,
            debug=True,
            host='0.0.0.0',
            port=5000)
